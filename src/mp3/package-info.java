/**
 * A cross platform and Open Source mp3 player library implemented in Java.
 * 
 * @author <a href="http://cristian.sulea.net" rel="author">Cristian Sulea</a>
 * @version 1.1, June 12, 2014
 */
package mp3;

