/*
 * Edit by Romain PETIT 05/04/2015
 *
 * Copyright (C) 2014 Cristian Sulea ( http://cristian.sulea.net )
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mp3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Abstract implementation for {@link MP3}.This provides a convenient base class
 * from which other applications can be easily derived.
 *
 * @author <a href="http://cristian.sulea.net" rel="author">Cristian Sulea</a>
 * @version 1.1, June 12, 2014
 */
public abstract class AbstractMP3 implements MP3 {

    /**
     * The value representing the volume when gain control is not changed or is
     * set to default value.
     */
    int DEFAULT_VOLUME = 100;
    
    /**
     *
     */
    protected volatile MP3Input input;

    /**
     *
     */
    protected final static boolean trace = true;

    /**
     *
     */
    protected final Log logger = LogFactory.getLog(getClass());

    /**
     *
     */
    protected final Object mutex = new Object();

    /**
     *
     */
    protected enum STATUS {
	PLAYING,
	PAUSED,
	STOPPED
    }

    /**
     *
     */
    protected MP3listener listener = null;

    /**
     *
     */
    protected STATUS status = STATUS.STOPPED;

    /**
     *
     */
    private int volume = DEFAULT_VOLUME;

    /**
     *
     * @param aMP3listener
     */
    @Override
    public void addListener(MP3listener aMP3listener) {
	listener = aMP3listener;
    }

    /**
     * Commence ou reprend la lecture (fonction abstraite 'playImpl')
     * Appel listener.onPlay()
     * status doit être défini à PLAYING dans la fonction abstraite 'playImpl' 
     */
    @Override
    public final void play() {
	if (trace) {
	    logger.info("AbstractMP3::play call listener.onPlay()");
	}
	synchronized (mutex) {
	    playImpl();	    
	    if (listener != null) {
		listener.onPlay(input);
	    }	    
	}
    }

    /**
     *
     */
    protected abstract void playImpl();

    /**
     *
     * @return <code>true</code> si la lecture est en cours
     */
    @Override
    public final boolean isPlaying() {
	synchronized (mutex) {
	    return status == STATUS.PLAYING;
	}
    }

    /**
     * Met en pause (appel listener.onPause())
     * status = PAUSED
     */
    @Override
    public final void pause() {
	if (trace) {
	    logger.info("AbstractMP3::pause call listener.onPause()");
	}
	synchronized (mutex) {
	    status = STATUS.PAUSED;
	    if (listener != null) {
	    listener.onPause(input);
	    }	    
	}
    }

    /**
     *
     * @return <code>true</code> si la lecture est en pause
     */
    @Override
    public final boolean isPaused() {
	synchronized (mutex) {
	    return status == STATUS.PAUSED;
	}
    }

    /**
     * Arrête la lecture (appel listener.onStop())
     * status = STOPPED
     */
    @Override
    public final void stop() {
	if (trace) {
	    logger.info("AbstractMP3::stop call listener.onStop()");
	}
	synchronized (mutex) {
	    status = STATUS.STOPPED;
	    if (listener != null) {
	    listener.onStop(input);
	}
	}
    }

    /**
     *
     * @return <code>true</code> si la lecture est arrêtée
     */
    @Override
    public final boolean isStopped() {
	synchronized (mutex) {
	    return status == STATUS.STOPPED;
	}
    }

    /**
     * Défini le volume
     * @param v
     */
    @Override
    public void setVolume(int v) {
	if (v < 0 || v > 2 * DEFAULT_VOLUME) {
	    throw new IllegalArgumentException("Wrong value for volume (" + v + "), must be in interval [0.." + (2 * DEFAULT_VOLUME) + "].");
	}
	if (trace) {
	    logger.info("AbstractMP3::volume(" + v + "%) call listener.onVolumeChange(oldVolume,newVolume)");
	}
	if (listener != null) {
	    listener.onVolumeChange(getVolume(), v);
	}
	this.volume = v;
    }

    /**
     * Récupère le volume
     * @return volume
     */
    @Override
    public int getVolume() {
	return this.volume;
    }

}
