package mp3;

/**
 *
 * @author Romain PETIT <tokazio@esyo.net>
 */
public abstract class MP3listener {
    
    public abstract void onEnd(MP3Input aMP3Input);
    public abstract void onPlay(MP3Input aMP3Input);
    public abstract void onPlaying(MP3Input aMP3Input, long aPosition, long aSize);
    public abstract void onPause(MP3Input aMP3Input);
    public abstract void onUnPause(MP3Input aMP3Input);    
    public abstract void onStop(MP3Input aMP3Input);
    public abstract void onVolumeChange(int oldValue,int newValue);
    public abstract void onError(MP3Input aMP3Input,String aMessage,Exception e);
    public void onError(MP3Input aMP3Input,String aMessage,Throwable t){
	onError(aMP3Input,aMessage,(Exception) t);
    }
    
}
