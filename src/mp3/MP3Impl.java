/*
 * Edit by Romain PETIT 05/04/2015
 *
 * Copyright (C) 2014 Cristian Sulea ( http://cristian.sulea.net )
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mp3;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.BooleanControl;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Line;
import javax.sound.sampled.SourceDataLine;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.SampleBuffer;
import static mp3.AbstractMP3.trace;

/**
 * Default implementation (using javazoom) for {@link MP3}.
 *
 * @author <a href="http://cristian.sulea.net" rel="author">Cristian Sulea</a>
 * @version 2.2, June 12, 2014
 */
class MP3Impl extends AbstractMP3 implements MP3 {

    /**
     * 
     */
    private volatile Thread thread;

    /**
     * 
     */
    private volatile SourceDataLine source;

    /**
     * 
     */
    private int sourceVolume = DEFAULT_VOLUME;

    /**
     * 
     */
    private final float[] sourceVolumes = new float[2 * DEFAULT_VOLUME + 1];

    /**
     *
     * @param input
     */
    public MP3Impl(MP3Input input) {
	this.input = input;
    }

    /**
     * ?
     */
    @Override
    public void join() {
	try {
	    thread.join();
	} catch (InterruptedException e) {
	    if (listener != null) {
		listener.onError(input, "failed to #join()", e);
	    }
	    if (trace) {
		logger.error("MP3impl::failed to #join()", e);
	    }
	}
    }

    /**
     *
     */
    @Override
    protected void playImpl() {
	// if paused, play will unpause
	if (status == STATUS.PAUSED) {
	    if (trace) {
		logger.info("MP3Impl::unpause");
	    }
	    // change the status to playing
	    status = STATUS.PLAYING;
	    // wake up the playing thread
	    mutex.notifyAll();
	    return;
	}
	// if playing, play will first stop then play
	if (status == STATUS.PLAYING) {
	    if (trace) {
		logger.info("MP3Impl::stop (forced stop before new play)");
	    }
	    // change the status to stopped
	    status = STATUS.STOPPED;
	    // do this on a new thread, or we will lock everything
	    Thread th = new Thread() {
		@Override
		public void run() {
		    // wait for the playing thread to finish
		    try {
			thread.join();
		    } catch (NullPointerException | InterruptedException e) {
			if (listener != null) {
			    listener.onError(input, "MP3impl::Hmm... The method Thread#join() failed?", e);
			}
			if (trace) {
			    logger.error("MP3impl::Hmm... The method Thread#join() failed?", e);
			}
		    }
		    // launch a new play command
		    play();
		}
	    };
	    th.setDaemon(true);
	    th.start();
	    return;
	}
	// it's a normal play command
	if (trace) {
	    logger.info("MP3Impl::play");
	}
	// if playing thread is not null, then the mp3 is already playing
	if (thread == null) {
	    // change the status to playing
	    status = STATUS.PLAYING;
	    // create and launch the playing thread
	    thread = new Thread() {
		@Override
		public void run() {
		    InputStream inputStream = null;
		    Bitstream soundStream = null;
		    try {
			inputStream = input.createStream();
			soundStream = new Bitstream(inputStream);
			Decoder decoder = new Decoder();
			// start the cycle
			// will end on stop command or on end of stream
			while (true) {
			    synchronized (mutex) {
				// if is a stop command
				// break the play
				if (status == STATUS.STOPPED) {
				    break;
				}
				// if is a pause command
				if (status == STATUS.PAUSED) {
				    // flush the audio data
				    if (source != null) {
					source.flush();
				    }
				    // and put the playing thread in wait for unpause
				    // TODO: daca se da stop dupa pauza, nu ramen in wait forever?
				    while (status == STATUS.PAUSED) {
					try {
					    mutex.wait();
					} catch (InterruptedException e) {
					    if (listener != null) {
						listener.onError(input, "MP3impl::The thread WAIT method failed, this means PAUSE will not work.", e);
					    }
					    if (trace) {
						logger.error("MP3impl::The thread WAIT method failed, this means PAUSE will not work.", e);
					    }
					}
				    }
				}
			    }
			    // play
			    try {
				// read the stream frame by frame
				Header frame = soundStream.readFrame();
				// if frame is null, the end of the stream has been reached
				if (frame == null) {
				    if (listener != null) {
					listener.onEnd(input);
				    }
				    break;
				} else {
				    // first time / first frame
				    // create the playing source if is needed
				    if (source == null) {
					int frequency = frame.frequency();
					int channels = (frame.mode() == Header.SINGLE_CHANNEL) ? 1 : 2;
					AudioFormat format = new AudioFormat(frequency, 16, channels, true, false);
					Line line = AudioSystem.getLine(new DataLine.Info(SourceDataLine.class, format));
					source = (SourceDataLine) line;
					source.open(format);
					source.start();
					// compute the source volumes
					try {
					    FloatControl gainControl = (FloatControl) source.getControl(FloatControl.Type.MASTER_GAIN);
					    float stepMinimum = Math.abs(gainControl.getMinimum() / DEFAULT_VOLUME);
					    float stepMaximum = gainControl.getMaximum() / DEFAULT_VOLUME;
					    sourceVolumes[0] = gainControl.getMinimum();
					    sourceVolumes[DEFAULT_VOLUME] = 0;
					    sourceVolumes[2 * DEFAULT_VOLUME] = gainControl.getMaximum();
					    for (int i = 1, n = DEFAULT_VOLUME - 1; i <= n; i++) {
						sourceVolumes[i] = sourceVolumes[i - 1] + stepMinimum;
					    }
					    for (int i = DEFAULT_VOLUME + 1, n = 2 * DEFAULT_VOLUME; i <= n; i++) {
						sourceVolumes[i] = sourceVolumes[i - 1] + stepMaximum;
					    }
					} catch (Throwable t) {
					    if (listener != null) {
						listener.onError(input, "MP3impl::Unexpected error calculating the source volumes.", t);
					    }
					    if (trace) {
						logger.error("MP3impl::Unexpected error calculating the source volumes.", t);
					    }
					    // in case of any error, set the source volume to 0
					    // meaning the signal's loudness is unaffected
					    for (int i = 0, n = 2 * DEFAULT_VOLUME; i <= n; i++) {
						sourceVolumes[i] = 0;
					    }
					}
				    }
				    // update the volume on the source
				    if (sourceVolume != getVolume()) {
					sourceVolume = getVolume();
					FloatControl gainControl = (FloatControl) source.getControl(FloatControl.Type.MASTER_GAIN);
					BooleanControl muteControl = (BooleanControl) source.getControl(BooleanControl.Type.MUTE);
					if (sourceVolume == 0) {
					    muteControl.setValue(true);
					} else {
					    muteControl.setValue(false);
					    gainControl.setValue(sourceVolumes[sourceVolume]);
					}
				    }
				    // play the frame
				    SampleBuffer output = (SampleBuffer) decoder.decodeFrame(frame, soundStream);
				    short[] buffer = output.getBuffer();
				    int offs = 0;
				    int len = output.getBufferLength();
				    source.write(toByteArray(buffer, offs, len), 0, len * 2);
				    // don't forget to close the frame
				    soundStream.closeFrame();
				}
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Unexpected problems while playing.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Unexpected problems while playing.", e);
				}
				break;
			    }
			    if (listener != null) {
				listener.onPlaying(input, getPosition(), getSize());
			    }
			}
		    } catch (IOException e) {
			if (listener != null) {
			    listener.onError(input, "MP3impl::Unable to get the stream from input: " + input.toString(), e);
			}
			if (trace) {
			    logger.error("MP3impl::Unable to get the stream from input: " + input.toString(), e);
			}
		    } finally {
			if (source != null) {
			    try {
				source.flush();
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Error flushing the playing source.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Error flushing the playing source.", e);
				}
			    }
			    try {
				source.stop();
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Error stopping the playing source.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Error stopping the playing source.", e);
				}
			    }
			    try {
				source.close();
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Error closing the playing source.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Error closing the playing source.", e);
				}
			    }
			}
			if (soundStream != null) {
			    try {
				soundStream.close();
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Error closing the sound stream.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Error closing the sound stream.", e);
				}
			    }
			}
			if (inputStream != null) {
			    try {
				inputStream.close();
			    } catch (Exception e) {
				if (listener != null) {
				    listener.onError(input, "MP3impl::Error closing the input stream.", e);
				}
				if (trace) {
				    logger.error("MP3impl::Error closing the input stream.", e);
				}
			    }
			}
		    }
		    // end of run() method, so thread will die after this
		    // don't forget to update status
		    // and to reset thread and source
		    status = STATUS.STOPPED;
		    thread = null;
		    source = null;
		}
	    };
	    thread.start();
	}
    }

    /**
     *
     * @return la position en frame
     */
    @Override
    public long getPosition() {
	if (source != null) {
	    return source.getFramePosition();//.getMicrosecondPosition() / 1000L;
	} else {
	    return 0L;
	}
    }

    /**
     *
     * @return la taille en frame
     * @deprecated Non implémenté
     */
    @Deprecated
    @Override
    public long getSize() {

	return -1;
    }

    /**
     *
     * @param ss
     * @param offs
     * @param len
     * @return
     */
    private byte[] toByteArray(short[] ss, int offs, int len) {
	byte[] bb = new byte[len * 2];
	int idx = 0;
	short s;
	while (len-- > 0) {
	    s = ss[offs++];
	    bb[idx++] = (byte) s;
	    bb[idx++] = (byte) (s >>> 8);
	}
	return bb;
    }

}
