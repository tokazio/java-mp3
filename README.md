# MP3 #

Basé sur [https://github.com/cristian-sulea/jatoo-mp3](https://github.com/cristian-sulea/jatoo-mp3)

Ecoute de MP3 avec [jlayer](http://www.javazoom.net/javalayer/javalayer.html) et [java.sound](http://docs.oracle.com/javase/7/docs/api/javax/sound/sampled/package-summary.html)/[openal](https://github.com/urish/java-openal).
Utilise [mp3agic](https://github.com/mpatric/mp3agic) pour la lecture des tags id3.

La branche 'master' est https://github.com/cristian-sulea/jatoo-mp3
La branche 'simplification' est ma version.